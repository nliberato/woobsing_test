<?php

namespace Database\Seeders;


// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
//use App\Models\Rol;
//use App\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(15)->create();
        
         
        $Administrador  =  Role::create(['name' => 'Administrador']);     // 1
        $Invitado       =  Role::create(['name' => 'Invitado']);          // 2
        $Desconocido    =  Role::create(['name' => 'Desconocido']);       // 3
        $Temporal       =  Role::create(['name' => 'Temporal']);          // 4

        $Insertar       =  Permission::create(['name' => 'Insertar']);    // 1
        $Actualizar     =  Permission::create(['name' => 'Actualizar']);  // 2
        $Listar         =  Permission::create(['name' => 'Listar']);      // 3
        $Eliminar       =  Permission::create(['name' => 'Eliminar']);    // 4
        $Descargar      =  Permission::create(['name' => 'Descargar']);   // 5
        $Subir          =  Permission::create(['name' => 'Subir']);       // 6
        $Imprimir       =  Permission::create(['name' => 'Imprimir']);    // 7
        $Ver            =  Permission::create(['name' => 'Ver']);         // 8


        $Administrador->givePermissionTo($Insertar);
        $Administrador->givePermissionTo($Actualizar);
        $Administrador->givePermissionTo($Listar);
        $Administrador->givePermissionTo($Eliminar);
        $Administrador->givePermissionTo($Descargar);
        $Administrador->givePermissionTo($Subir);
        $Administrador->givePermissionTo($Imprimir);
        $Administrador->givePermissionTo($Ver);


        $Insertar ->assignRole($Administrador);
        $Actualizar ->assignRole($Administrador);
        $Listar ->assignRole($Administrador);
        $Eliminar ->assignRole($Administrador);
        $Descargar ->assignRole($Administrador);
        $Subir ->assignRole($Administrador);
        $Imprimir ->assignRole($Administrador);
        $Ver ->assignRole($Administrador);


        $userAdmin = User::create(['name'            => 'Admin',
                                    'last_name'       => 'Admin', 
                                    'email'           => 'admin@woobsingtest.com', 
                                    'phone'           => '1234567', 
                                    'password'        => Hash::make('password'),
                                    'remember_token'  => 'password',
                                    ]);
        $userAdmin -> assignRole([$Administrador,1]);


    }
}
